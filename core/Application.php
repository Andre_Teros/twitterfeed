<?php

namespace Core;

use Symfony\Component\HttpFoundation\Request;

class Application
{
    private static $request;
    private static $status = 200;
    private static $run = true;

    /**
     * Start application
     *
     * @return void
     */
    public static function start(): void
    {
        self::setHeaders();
        self::routeInit();
    }

    /**
     * Method returns application request
     *
     * @return Request
     */
    public static function getRequest(): Request
    {
        return self::$request;
    }

    /**
     * Method sets application request
     *
     * @param Request $request
     *
     * @return void
     */
    public static function setRequest(Request $request): void
    {
        self::$request = $request;
    }

    /**
     * Method returns response status
     *
     * @return int
     */
    public static function getStatus(): int
    {
        return self::$status;
    }

    /**
     * Method returns sets status
     *
     * @param int $status
     *
     * @return void
     */
    public static function setStatus(int $status): void
    {
        self::$status = $status;
    }

    /**
     * Method returns application state
     *
     * @return bool
     */
    public static function isRun(): bool
    {
        return self::$run;
    }

    /**
     * Method sets application state
     *
     * @param bool $run
     *
     * @return void
     */
    public static function setRun(bool $run): void
    {
        self::$run = $run;
    }

    /**
     * Method initialized routing configuration
     *
     * @return void
     */
    private static function routeInit(): void
    {
        $oRoute = new Route(__DIR__ . '/../config/routes.php');
        $oRoute->init();
    }

    /**
     * Method sets response headers
     *
     * @return void
     */
    private static function setHeaders():void
    {
        header("Content-type:application/json");
    }
}
