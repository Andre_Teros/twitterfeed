<?php

namespace Core;

class SQLiteConnect
{
    private const CONFIG_PATH = '/config/DBConfig.php';

    private static $instance;
    private static $oPdo;

    private function __construct()
    {
        $path = require APP_DIR . self::CONFIG_PATH;
        self::$oPdo = new \PDO('sqlite:' . $path);
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function getOPdo()
    {
        return self::$oPdo;
    }
}
