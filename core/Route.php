<?php

namespace Core;

use App\Controllers\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Exception;

class Route
{
    private $routesConfig;

    private $baseControllerClass = BaseController::class;

    private $actionMethod;

    private $actionParams = [];

    /**
     * Route constructor
     *
     * @param $configFile
     */
    public function __construct($configFile)
    {
        $this->routesConfig = $configFile;
    }

    /**
     * Route initialization
     *
     * @return void
     */
    public function init(): void
    {
        $request = Request::createFromGlobals();
        $routes = include $this->routesConfig;

        $context = new Routing\RequestContext();
        $context->fromRequest($request);
        $matcher = new Routing\Matcher\UrlMatcher($routes, $context);

        try {
            Application::setRequest($request);
            $attributes = $matcher->match($request->getPathInfo());
            $response = $this->callAction($attributes);
        } catch (ResourceNotFoundException $exception) {
            $response = new Response(\json_encode(['error' => 'Not Found']), 404);
        } catch (Exception $exception) {
            $response = new Response(\json_encode(['error' => 'internal error']), 500);
        }

        $response->send();
    }

    /**
     * Method calls controller's action method by request
     *
     * @param array $attributes
     *
     * @return Response
     */
    private function callAction(array $attributes): Response
    {
        $controllerClass = $attributes['_controller'][0];
        if (!\class_exists($controllerClass) || !\is_subclass_of($controllerClass, $this->baseControllerClass)) {
            throw new ResourceNotFoundException();
        }

        $oController = new $controllerClass();

        $this->actionMethod = $attributes['_controller'][1];
        if (!\method_exists($oController, $this->actionMethod)) {
            throw new ResourceNotFoundException();
        }

        $this->collectParamsForAction($attributes);

        ob_start();
        foreach ($this->getMethodCallList() as $method => $params) {
            if (Application::isRun()) {
                $oController->{$method}(...$params);
            }
        }

        return new Response(ob_get_clean(), Application::getStatus());
    }

    /**
     * Method returns method sequence
     *
     * @return array
     */
    private function getMethodCallList(): array
    {
        return [
            BaseController::BEFORE_ACTION_METHOD => [],
            $this->actionMethod => $this->actionParams,
            BaseController::AFTER_ACTION_METHOD => [],
        ];
    }

    /**
     * Method collects parameters for action method from request
     *
     * @param array $attributes
     *
     * @return void
     */
    private function collectParamsForAction(array $attributes): void
    {
        foreach ($attributes as $name => $attribute) {
            if (strpos($name, '_') === false) {
                $this->actionParams[] = $attribute;
            }
        }
    }
}
