<?php

namespace App\Controllers;

abstract class BaseController
{
    public const BEFORE_ACTION_METHOD = 'beforeAction';
    public const AFTER_ACTION_METHOD = 'afterAction';

    /**
     * Method called before action
     *
     * @return void
     */
    public function beforeAction(): void
    {
    }

    /**
     * Method called after action
     *
     * @return void
     */
    public function afterAction(): void
    {
    }
}
