<?php

namespace App\Controllers;

use App\Models\User;
use App\Services\Twitter;
use App\Services\TwitterServiceException;
use Core\Application;
use Exception;

class FeedController extends BaseController
{
    private const ID_LENGTH = 32;

    private $getParams = [];

    /**
     * Method adds Twitter user to the feed
     */
    public function add()
    {
        if (empty($this->getParams['user'])) {
            $this->missingParamsError();
            return;
        }

        $model = new User();
        try {
            $model->add($this->getParams['user']);
        } catch (Exception $e) {
            $this->internalError($e->getMessage());
            return;
        }

        echo \json_encode(['success' => 'user added']);
    }

    /**
     * Method gets the latest tweets from users that were added to feed
     */
    public function feed()
    {
        $model = new User();
        try {
            $users = $model->find();
        } catch (Exception $e) {
            $this->internalError($e->getMessage());
            return;
        }

        if (empty($users)) {
            Application::setRun(false);
            echo \json_encode(['error' => 'No users in feed']);
            return;
        }

        $twitterCreds = require APP_DIR . '/config/twitterCreds.php';
        $oTwitter = new Twitter($twitterCreds);

        $result = [];
        foreach ($users as $user) {
            try {
                $result[] = $oTwitter->getShortTweetsByUser($user['name']);
            } catch (TwitterServiceException $e) {
                $result[] = [[
                    'user' => $user['name'],
                    'error' => $e->getMessage()
                ]];
            }
        }

        echo json_encode($result);
    }

    /**
     * Method removes Twitter user from the feed
     */
    public function remove()
    {
        if (empty($this->getParams['user'])) {
            $this->missingParamsError();
            return;
        }

        $model = new User();
        try {
            $model->remove($this->getParams['user']);
        } catch (Exception $e) {
            $this->internalError($e->getMessage());
            return;
        }

        echo \json_encode(['success' => 'user removed']);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeAction(): void
    {
        $this->getParams = Application::getRequest()->query->all();

        if (!$this->checkGetParams()) {
            return;
        }
        $this->checkSecurity();
    }

    /**
     * Method validate GET parameters
     *
     * @return bool
     *
     */
    private function checkGetParams(): bool
    {
        if (empty($this->getParams['id']) || empty($this->getParams['secret'])) {
            $this->missingParamsError();
            return false;
        }

        if (\strlen($this->getParams['id']) !== self::ID_LENGTH) {
            Application::setRun(false);
            echo \json_encode(['error' => '\'id\' parameter must have ' . self::ID_LENGTH . ' characters']);
            return false;
        }

        return true;
    }

    /**
     * Method checks security parameters
     *
     * @return void
     */
    private function checkSecurity(): void
    {
        $this->getParams['user'] = $this->getParams['user'] ?? '';

        if (\sha1($this->getParams['id'] . $this->getParams['user']) !== $this->getParams['secret']) {
            $this->authError();
        }
    }

    /**
     * Method stop application and show missing parameter error
     *
     * @return void
     */
    private function missingParamsError(): void
    {
        Application::setRun(false);
        echo \json_encode(['error' => 'missing parameter']);
    }

    /**
     * Method stop application and show access denied error
     *
     * @return void
     */
    private function authError(): void
    {
        Application::setRun(false);
        Application::setStatus(401);
        echo \json_encode(['error' => 'access denied']);
    }

    private function internalError(string $additionalInformation = '')
    {
        Application::setRun(false);
        Application::setStatus(500);
        $message = 'internal error';
        if (!empty($additionalInformation)) {
            $message .= ': ' . $additionalInformation;
        }
        echo \json_encode(['error' => $message]);
    }
}