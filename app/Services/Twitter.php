<?php

namespace App\Services;

class Twitter
{
    public const TWEETS_BY_USER = 3;

    private $creds;

    /**
     * Twitter constructor.
     *
     * $creds['consumer_key']
     * $creds['consumer_secret']
     * $creds['oauth_access_token']
     * $creds['oauth_access_token_secret']
     *
     * @param array $creds
     */
    public function __construct(array $creds)
    {
        $this->creds = $creds;
    }

    /**
     * @param string $userName
     *
     * @return array
     * @throws TwitterServiceException
     */
    public function getShortTweetsByUser(string $userName): array
    {
        $query = [
            'count' => self::TWEETS_BY_USER,
            'screen_name' => $userName
        ];

        $rawInfo = $this->getRawTweetsByQuery($query);

        if (!empty($rawInfo['error'])) {
            throw new TwitterServiceException($rawInfo['error']);
        }

        if (!empty($rawInfo['errors'])) {
            throw new TwitterServiceException($rawInfo['errors'][0]['message'], $rawInfo['errors'][0]['code']);
        }

        $result = [];

        foreach ($rawInfo as $tweet) {
            $result[] = [
                'user' => $tweet['user']['screen_name'],
                'tweet' => $tweet['text'],
                'hashtag' => array_column($tweet['entities']['hashtags'], 'text')
            ];
        }

        return $result;
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function getRawTweetsByQuery($query)
    {
        $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

        $consumer_key = $this->creds['consumer_key'];
        $consumer_secret = $this->creds['consumer_secret'];
        $oauth_access_token = $this->creds['oauth_access_token'];
        $oauth_access_token_secret = $this->creds['oauth_access_token_secret'];

        $oauth = [
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $oauth_access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        ];

        $base_params = empty($query) ? $oauth : array_merge($query, $oauth);
        $base_info = $this->buildBaseString($url, 'GET', $base_params);
        $url = empty($query) ? $url : $url . '?' . http_build_query($query);

        $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;

        $header = [$this->buildAuthorizationHeader($oauth), 'Expect:'];
        $options = [
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
        ];

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);
        return json_decode($json ,true);
    }

    /**
     * @param $baseURI
     * @param $method
     * @param $params
     *
     * @return string
     */
    private function buildBaseString($baseURI, $method, $params): string
    {
        $result = [];
        ksort($params);
        foreach ($params as $key => $value) {
            $result[] = "$key=" . rawurlencode($value);
        }
        return $method . '&' . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $result));
    }

    /**
     * @param $oauth
     *
     * @return string
     */
    private function buildAuthorizationHeader($oauth): string
    {
        $result = 'Authorization: OAuth ';
        $values = [];
        foreach ($oauth as $key => $value) {
            $values[] = "$key=\"" . rawurlencode($value) . '"';
        }
        $result .= implode(', ', $values);
        return $result;
    }
}