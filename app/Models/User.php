<?php

namespace App\Models;

use PDO;
use Exception;

class User extends BaseModel
{
    /**
     * @return array
     * @throws Exception
     */
    public function find(): array
    {
        $sql = '
            SELECT name
            FROM users
        ';
        if(!$rs = $this->getODB()->query($sql)) {
            throw new Exception(end($rs->errorInfo()));
        }

        return $rs->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $name
     *
     * @return bool
     * @throws Exception
     */
    public function add(string $name): bool
    {
        $sql = '
            INSERT INTO users (name)
            VALUES (?);
        ';

        $prepareSql = $this->getODB()->prepare($sql);
        if (!$prepareSql->execute([$name])) {
            $aErrors = $prepareSql->errorInfo();
            throw new Exception(end($aErrors));
        }

        return true;
    }

    /**
     * @param $name
     *
     * @return bool
     * @throws Exception
     */
    public function remove(string $name): bool
    {
        $sql = '
            DELETE FROM users
            WHERE name = ?;
        ';

        $prepareSql = $this->getODB()->prepare($sql);
        if (!$prepareSql->execute([$name])) {
            $aErrors = $prepareSql->errorInfo();
            throw new Exception(end($aErrors));
        }

        return true;
    }
}
