<?php

namespace App\Models;

use Core\SQLiteConnect;

class BaseModel
{
    private $oDB;

    public function __construct() {
        $this->oDB = SQLiteConnect::getInstance()::getOPdo();
    }

    public function getODB(): \PDO {
        return $this->oDB;
    }
}