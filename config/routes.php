<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use App\Controllers\FeedController;

$routes = new RouteCollection();

$routes->add('feed_add', (new Route('/add', array(
    '_controller' => [FeedController::class, 'add']
)))->setMethods('get'));

$routes->add('feed_feed', (new Route('/feed', array(
    '_controller' => [FeedController::class, 'feed']
)))->setMethods('get'));

$routes->add('feed_remove', (new Route('/remove', array(
    '_controller' => [FeedController::class, 'remove']
)))->setMethods('get'));

return $routes;
