<?php

define('APP_DIR', __DIR__);

require_once APP_DIR . '/vendor/autoload.php';

\Core\Application::start();